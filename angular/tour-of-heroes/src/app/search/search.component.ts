import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
//Un Subject est un Observable dans lequel on peut envoyer des donners qui seront destribuées à plusieurs observateurs
//On peut ainsi y entrer les données de notre champ de recherche comme dans un observable mais qui serait également une source d'observable
  private searchTerms = new Subject<string>();
  //par convention on rajoute un $ à la fin d'un nom de variable contenant un observable
  heroes$: Observable<Hero[]>;

  constructor(private heroService: HeroService) {}

  ngOnInit(): void {
    //searchTerms étant un observable d'observables on peut jeter un oeil à ces observables via pipe()
    this.heroes$ = this.searchTerms.pipe(
      //debounceTime() permet de mettre en place un minuteur ne laissant passer les données qu'après un certain délai
      debounceTime(150),
      //distinctUntilChanged permet de ne pas considérer la dernière donée en date si elle est identique à la précédente
      distinctUntilChanged(),
      //switchMap se comporte comme map mais ne considére que le dernier observable récupéré, ce qui permet de renvoyer un observable au lieu d'un observable <observable> et également de se débarasser des valeurs obsolétes
      switchMap((term:string) => this.heroService.searchHeroes(term))
    )
  }

  search(term: string): void {
    this.searchTerms.next(term);
  }

}
