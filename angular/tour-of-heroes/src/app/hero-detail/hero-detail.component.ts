import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
    selector: 'app-hero-detail',
    templateUrl: './hero-detail.component.html',
    styleUrls: ['./hero-detail.component.css'],
})
export class HeroDetailComponent implements OnInit {
    hero?: Hero;

    constructor(
        private route: ActivatedRoute,
        private heroService: HeroService,
        private location: Location
    ) {}

    ngOnInit(): void {
        //pour pouvoir r&agir aux changements dans notre route lorsque le component est déjà monté
        // on souscrit à l a parammap au lieu de lire la valeur depuis un snapshot figé dans le temps
        this.route.paramMap.subscribe((params) => {
            const id = +params.get('id');
            this.heroService
            .getHero(id)
            .subscribe((heroData) => (this.hero = heroData));
        });
    }

    goBack(): void {
        this.location.back();
    }

    save(): void {
        //en appelant goBack dans notre fonction d'appel de l'observable on permet à l'utilisateur de revenir automatiquement en arrière lorsque la mise à jour a été effectuée
        this.heroService.update(this.hero).subscribe(() => this.goBack());
    }
}
