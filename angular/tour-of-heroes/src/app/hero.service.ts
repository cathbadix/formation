import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { Hero } from './hero';
import { MessageService } from './message.service';
import { HEROES } from './mock-heroes';

@Injectable({
    providedIn: 'root',
})
export class HeroService {
    constructor(
        private http: HttpClient,
        private messageService: MessageService
    ) {}

    private apiUrl = 'api/heroes';

    getHeroes(): Observable<Hero[]> {
        return (
            this.http
                .get<Hero[]>(this.apiUrl)
                //pipe permet d'entrer dans l'observable pour pouvoir y effectuer des opérations lors des différentes étapes de la vie de la donnée (donnée reçue, erreur, etc)
                .pipe(
                    //tap est un opérateur permettant de jeter un oeil à la donnée reçue dans l' observable sans la modifier et sans l'intercepter, de façon à ce qu'elle puisse continuer sa route vers les souscripteurs (notre component dans lequel "subscribe" est appelé)
                    tap((_) =>
                        //une fois qu'on est bien sûr que la donnée est arrivée on peut envoyer notre message
                        this.log('fetched heroes list')
                    )
                )
        );
    }

    getTopHeroes(): Observable<Hero[]> {
        return this.getHeroes().pipe(
            map((heroes) => {
                this.log('fetched top heroes');
                return heroes
                    .sort((a: Hero, b: Hero) => b.rating - a.rating)
                    .slice(0, 3);
            })
        );
    }

    getHero(id: number): Observable<Hero> {
        return this.http.get<Hero>(`${this.apiUrl}/${id}`).pipe(
            tap((hero) => {
                this.log(`fetched hero #${hero.id}`);
            })
        );
    }

    add(hero: Hero): Observable<Hero> {
        return this.http
            .post<Hero>(this.apiUrl, hero)
            .pipe(tap((hero) => this.log(`hero ${hero.id} added`)));
    }

    delete(hero: Hero): Observable<Hero> {
        return this.http
            .delete<Hero>(`${this.apiUrl}/${hero.id}`)
            .pipe(tap((_) => this.log(`hero ${hero.id} deleted`)));
    }

    update(hero: Hero): Observable<Hero> {
        return this.http
            .put<Hero>(`${this.apiUrl}/${hero.id}`, hero)
            .pipe(tap((_) => this.log(`hero ${hero.id} updated`)));
    }

    log(message: string) {
        this.messageService.add(`HeroService : ${message}`);
    }

    searchHeroes(term: string): Observable<Hero[]> {
        if(!term.trim()){
            return of([]);
        }
        return this.http.get<Hero[]>(`${this.apiUrl}/?name=${term}`).pipe(
            tap ( heroes => this.log(`search: ${heroes.length} hero found`)) 
        )
    }
}
