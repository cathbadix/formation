<?php 

//je vérifie que les champs suivants sont prèsent
if (isset($_POST['nickname']) && isset($_POST['password'])){
    //pour plus de clareté, je range mes variables $_POST dans des variable plus courtes
    $nickname = $_POST['nickname'];
    $password = $_POST['password'];

    if (!empty($nickname) && !empty($password)){
        
        $dbh = new PDO('mysql:host=localhost;dbname=blog; charset=utf8', 'root', '');
        //je prépare ma requête sql afin de controler que l'utilisateur existe
        $get_user = "SELECT* FROM `users` WHERE nickname = :nickname";
        $stmt =$dbh->prepare($get_user);

        $stmt->execute([
            ":nickname" => $nickname
        ]);

        //on récupére notre ligne de résultat
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($user){ //si on récupére quelque chose
            //on vérifie le mot de passe avec password_verify qui va comparer le mot de passe en claire avec le mot de passe haché de la bdd
            if (password_verify($password, $user["password"])) {
                //si le mot de passe est bien le bon, on peut commencer une session, ou rediriger ou faire ce qu'on veut
                session_start();
                //on enregistre l'id de l'utilisateur dans la session
                $_SESSION["userid"] = $user["id"];
                header('Location: index.php');
                
            } else {
                echo "nom d'utilisateur ou mot de passe erronés";
            }
        } else {
            echo "nom d'utilisateur ou mot de passe erronés";
        }
    } else {
        echo "erreur de champ vide";
    }
} else {
    echo "erreur formulaire";
}
?>