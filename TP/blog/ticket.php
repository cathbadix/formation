<?php

session_start();

if (isset($_SESSION['userid'])) {
    $userid = $_SESSION['userid'];

    if (isset($_POST['title']) && isset($_POST['content'])){

        $title = $_POST['title'];
        $content = $_POST['content'];
    
        if (!empty($title) && !empty($content)){
    
            $dbh = new PDO('mysql:host=localhost;dbname=blog; charset=utf8', 'root', '');
            $create_ticket = "INSERT INTO `tickets`(`title`, `content`, `date_create`, `users_id`) VALUES (:title, :content, :date_create, :users_id)";
    
            $stmt = $dbh->prepare($create_ticket);
    
            $date_create = date('Y-m-d h:i:s');
            $users_id = $_SESSION['userid'];
    
            $stmt->execute([
                ":title" => $title,
                ":content" => $content,
                ":date_create"=> $date_create,
                ":users_id"=> $users_id            
            ]);
            //on redirige
            header('Location: index.php');
        } else {
            echo "erreur de champ vide";
        }
    } else {
        echo "erreur de formulaire";

}























}
?>