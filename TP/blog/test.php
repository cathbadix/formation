<?php

$dbh = new PDO('mysql:host=localhost;dbname=blog; charset=utf8', 'root', '');

$get_ticket ="SELECT * FROM `tickets` ORDER BY `tickets`.`date_create` DESC LIMIT 5";

$stmt = $dbh->prepare($get_ticket);

$stmt->execute();

$ticket = $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach ($ticket as $ligne) {
    $date_create = date('Y-m-d h:i:s');
    echo sprintf(
        "
            <div class=ticket>
                <span class='title'>
                    <h3>%s</h3>
                </span>
                <span class='date'>                    
                    %s,
                </span>
                <span class='content'>
                    <p>%s</p></div>
                </span>
        ",
            
        $ligne['title'],
        $ligne['date_create'], 
        $ligne['content'] 
    );

}




