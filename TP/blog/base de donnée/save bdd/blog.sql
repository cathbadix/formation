-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 22 déc. 2020 à 12:16
-- Version du serveur :  10.4.14-MariaDB
-- Version de PHP : 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comcontent` longtext NOT NULL,
  `date_comcontent` datetime NOT NULL DEFAULT current_timestamp(),
  `users_id` int(11) NOT NULL,
  `tickets_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date_create` datetime NOT NULL DEFAULT current_timestamp(),
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tickets`
--

INSERT INTO `tickets` (`id`, `title`, `content`, `date_create`, `users_id`) VALUES
(1, 'Bonjour, voici mon blog', 'Vous trouverez ici toute l\'actualité me concernant. Mes centres d\'intérêts, mes points de vues, mes projets.  ', '2020-12-20 01:38:02', 1),
(2, 'Mercenaire Red Shadow', '1- BACKGROUND:  Créé en 1893 par McCarty Clifford, la Red Shadow est un groupe composé de Mercenaire. Fils d’ancien Marshall, McCarty est aussi ancien chasseur de prime. Durant cette exercice, il fît la rencontre de Cheyenne, chef d’un groupe de Mercenaire nommé La “Black Shadow” en 1890.   Ce dernier fut condamné à mort par les Sheriffs du Dakota du Nord pour divers faits dont il n’était pas coupable. McCarty le sauve le jour de son exécution par des Shérifs.  Cela faisait quelque temps déjà que McCarty surveillait le groupe et son activité, et ceux au vue de la prime sur leurs têtes. Mais ces derniers étant quasi intouchable dans cette région, McCarty pris son temps en patience pour analyser ces derniers.  Après des mois de surveillances et d’enquêtes, Clifford comprit que la bande de Cheyenne, ni Cheyenne lui même étaient à l’origine des chefs d’inculpations mis en avant par les Sheriffs, mais étaient visés par ces derniers pour éradiquer leurs influences sur les citoyens.  Les Sheriffs étaient des personnes corrompues, n\'hésitant pas à utiliser leurs pouvoirs et leurs positions, pour assouvir leurs méfaits et de les reprocher à certains citoyens pour se dédouaner.   C’est pour cela que Clifford prit la décision de leur venir en aide, même si ces derniers n’étaient pas des enfants de cœurs, il était hors de question pour Clifford que les Sheriff s’en sortent en faisant porter le chapeau de leurs méfaits à d’autres. L’honneur est pour McCarty plus important que tout et toute cette histoire amena Clifford à changer clairement sa façon de voir les choses sur le bien et le mal.   Cheyenne fut reconnaissant envers Clifford pour avoir sauvé sa vie et celles de ces hommes, ce qui les amena à s\'associer. Pendant prêt de 3 ans, Clifford s’occupait de la gestion du groupe, tant sur le plan économique que leurs actions. Clifford reçut son surnom de Red, pour sa passion pour la couleur rouge notamment et était vue et perçue par les gens comme l’ombre du groupe qui veillait sur eux et leurs intérêts.  En 1893, par un soir de printemps, Cheyenne et sa bande se retrouvent pris au piège par le groupe de Shériff en question.  Ces derniers, n’arrivant pas à les liquider à l’aide de leurs positions de Shérifs et par rapport à la renommé du groupe dans l’état , ils prirent la décision de les conduires dans un piège pour en finir avec eux et être enfin libre de pouvoir reprendre le contrôle des affaires détenue par le groupe de Cheyenne.  Lors de cet affrontement, les hommes de Cheyenne perdirent la vie et Cheyenne fut gravement blessé. Néanmoins, il put prendre la fuite et alla se réfugier chez son ami Clifford McCarty, partie dans son Montana Natal pour y régler quelques problèmes.  Au bout de 4 jours de cheval à travers le Dakota et le Montana, Cheyenne arriva chez Clifford à Jefferson City dans le montana, et y raconta ce qui c’était passé. Malgré les soins reçus et le repos, ce dernier décéda du grosse infection et en mourra dans les bras de son amie quelques jours plus tard. Lui laissant comme dernier héritage une lettre et ces précieux éperons…    Pris de chagrin, pour la disparition de son ami, Clifford alimenta sa haine envers les Sheriffs du Dakota et créa La “Red Shadow”. Un groupe composé exclusivement de mercenaires, ceci dans le but de venger la mort de Cheyenne et de nettoyer la corruption dans les forces de l’ordre présent dans ce comté.  Il recruta donc son Frère Jack McCarty et des amis d’enfances comme Clyde, Jesse, Spencer et consort, et prirent la direction du Dakota du Nord afin de venger la mort de Cheyenne et d’honorer sa mémoire. Pendant plus d’un an, le groupe faisait la lumière sur les agissements illégaux des Sheriffs, dans le seul but d’en faire des hors la lois pour pouvoir ensuite les attraper en toute légalité, de les torturer et de les tuer. Chaque Sheriff recevait une mort thématique pour les crimes qu’ils avaient fait par le passé. Après une année de traque à travers tout le Dakota du Nord et du Sud, l’ensemble des Sheriffs corrompus ayant ou non participé à la mort de Cheyenne fut liquidé.  Mais voilà, les petits groupes, faute de représentants de la loi, ni du groupe à Cheyenne qui les gérer, essayèrent de prendre possession des biens des civils par la force et la peur. Ce qui ne fut pas du goût de Clifford McCarty. Ce dernier créa donc divers branches de la Red Shadow et les envoya dans différents États des États-Unis, ceci dans le but d\'éradiquer la corruption dans les forces de l’ordre et de faire le sale travail pour ces derniers quant il le fallait pour le bien de ceux faisant respecter la loi. Même si les actions de la Red Shadow n’était pas toujours légal, elle n’avait jamais pour but d’être simplement bénéfique pour le groupe  Clifford s\'occupa aussi de gérer la montée en puissance des groupes de bandits afin de s\'assurer que ces derniers ne deviennent pas des chiens fous sans foi, sans honneur et sans limite.   Clifford avait bien compris qu’il fallait aussi bien des criminels que des représentants de la loi et que si l’un existait est pas l’autre, ce monde serait d’un ennui mortel. Il existe à ses yeux des mals nécessaires à la société, mais que ce mal devait craindre tout de même quelque chose.  En 6 ans, la Red Shadow s’implanta dans plus de 40 États des État-Unis, rassemblant plus de 900 à 1000 hommes dispersés dans ces derniers jurant fidélité et dévouement à la Red Shadow.  Mais voilà, les hautes instances de notre pays ne voyaient pas forcément cette émergence d’un bon œil et commencèrent à faire la chasse aux sorcières auprès de ces derniers, afin de reprendre sous leurs joues le pouvoir dans certains états ou il l’avait perdu.  Un jour d\'automne 1898, les Pinkertons débarquèrent dans le Saloon du groupe dans le Montana. Ces derniers avaient reçu pour ordre de chasser le groupe du territoire du Montana, il avait reçu comme consigne de proposer à McCarty soit de finir en prison et de répondre de leurs crimes, soit de dissoudre son groupe et de quitter les régions conquises, sinon ils seraient chassé, capturé et pendu.  Le groupe se voyait offrir cette proposition en remerciement des services rendus, mais ce temps doit être révolu. Le gouvernement ne pouvait tolérer que se soit un groupe de Mercenaire qui fasse la pluie et le beau temps et non la loi et les forces de l’ordre.  Au vu de la puissance militaire dépêchée par le gouvernement, Clifford prit la décision de se plier à cette volonté quitta son Montana Natal et parcourut les État Unis à la recherche d’un Eldorado où on pourrait avoir besoin de lui.  C’est donc comme cela que Clifford  arriva en Lousane et Plaine Syvany tout seul afin de se faire oublier, mais tout le monde le sait, le passé nous rattrape toujours...                     2- ORGANIGRAMME:      - BOSS:   	McCARTY Clifford  - BRAS DROIT:   	McCARTY Jack (pas encore Whitelist).  - CHEF:   	CURRY Clyde .  - MEDECIN:   	jesse (pas encore whitelist).  - HOMME DE MAIN:    	Shaenad (pas encore whitelist). 	Warrior (pas encore whitelist). 	VACANT. 	VACANT.     3- NOS ACTIONS:      Nous ne sommes pas vraiment un groupe illégal à proprement parlé, bien sur nous ne sommes pas blanc comme neige, mais nous ne chercherons pas à nuir aux forces de l’ordre. Ni à qui que ce soit en vérité, nous voulons juste proposer diverses actions afin de fournir du contenu à ceux et celles qui le désirent.  Au début nous voulions récupérer l’Opium qui était totalement délaissé par l’illégal pour sa faible rentabilité. On voulait justement voir pour le développer et proposer aux Marshalls  diverses courses  poursuites et actions avec les hommes de mains, sur du quasi full loose, pour leurs donner de l’activité. Ceci aussi dans le but de les voir venir me voir pour me signaler le comportement de mes ouvriers et créer donc forcément quelques actions, sanctions de ma part sur eux devant ces derniers pour faire bouger un peu tout cela. Malheureusement l’opium n’est plus, de ce fait on va essayer de trouver autres choses.  On va proposer une possibilité de blanchiment d’argent, avec des intérêts bien sûr et des seuils, pour ne pas avantager, déséquilibrer, et fausser l’économie du serveur. C’est d’ailleur pour cette raison que tous les membres du groupe ne se verront pas offrir tout à leurs arriver et devront par le travail l’obtenir, ceci dans le but, de rester Fairplay vis à vis des groupes illégaux en place ou en cours de création et qui ne disposerait pas comme de la possibilité de sauter des étapes. Je n’ai jamais aimé les groupes full armés qui sorte de nul par du jour au lendemain ce n’est pas pour le faire à mon tour.  Notre but sera donc de permettre au petit de pouvoir trouver un appuis pour se développer et au gros de trouver un endroit pour faire des affaires et blanchir par exemple leurs argents .  Les prêts seront donc encadrés et non abusifs. Exemple: 50 à 100 dollars pour un civil Lambda et voir 150 200 dollars pour un groupe illégal. Ils auront donc un contrat à respecter stipulant le temps maximum pour le remboursement, une obligation de justifier leurs prêts “ils peuvent nous mentir, sa donnera des raisons de les attrapers” et de la sommes total à rembourser “ ils peuvent ne pas le faire, ça nous donnera l’occasion de casser quelques phalange ou plus). Les prêts supérieurs peuvent éventuellement être fait avec un dépôt par exemple. Une personne lambda souhaitant un prêt de 150 dollars soit 50 dollars en plus du cap max, le pourra en échange par exemple d’un dépôt: une arme un cheval, une charrette ayant une valeur supérieur au prêt et permettant de s\'assurer du remboursement de ce dernier.  Dans le cas où la personne ne pourrait rembourser ou viendrait à décéder, les dépôts deviendront notre propriété on pourra en disposer comme bon nous semble.  Tout comme les prêts, le blanchiment d’argent sera capé, je pensais à blanchir plus ou moins les mêmes sommes que pour un prêt, mais je pense que pour ces caps, le mieux serait de voir avec vous, pour en décider comme un accord.  Il est important que cela n’affecte pas l’économie et l’équilibre non plus, le but étant d’être une sorte de tremplin pour les groupes et de leur permettre d’assumer leurs actes plutôt que de tout cacher.   Contrôler une drogue et la développer pour la rendre attrayante afin qu’on soit visé par quelques attaques. On peut trouver des accords avec les groupes criminels pour qu’ils paient leurs taxes en contrebande par exemple, ce qui nous permettra d\'envoyer nos hommes de main en ville pour la vendre sous le nez des Marshalls et de se faire taper sur les doigts par ces derniers.                                      4- PROJET COURS? MOYEN ET LONG TERME:     Je vais être franc, on ne prétend à rien, on ne souhaite pas grand chose et on n\'est pas là pour gagner non plus. On veut juste profiter de ce qu’on nous offre en RP, saisir les occasions qui nous seront mises à dispositions et nous laisser guider par les actions et réaction de chacun et chacune.  Le seul but à long terme que nous pouvons éventuellement avoir serait peut être de trouver un moyen de trouver une place à la tête d’une mairie par exemple.  Bref à voir.                 5- NOS BESOINS:     Réellement, nous faisons partie des gens qui n’avons pas besoin de quelques chose en particulier pour faire du RP, bien sûr nous pourrions peut être avoir besoins de quelques outils, mais avant de vous demander de consacrer du temps pour nous, je pense qu’il est de notre devoir déjà de vous montrez ce que l’on peut proposer et éventuellement lors d’un possible débriefing dans les semaines à venir et en fonctions de ce qu’il se passera en RP, vous demander peut-être un où deux outils ou agréments.  Actuellement, la seule chose qui me viendrait à l’idée, serait d’avoir quelques tentes autour de notre camp d’orpaillage, montrant de ce fait notre côté nomade et de faire comprendre aux autres qu’ici c’est chez nous et que c’est surveiller nuit et jour. Mais sachez que nous pouvons clairement nous passer de ceci pour le moment et donc vous concentrez sur des choses plus essentielles pour le serveur.            6- COMMANDEMENTS:    1 - Le Boss dispose du droit de vie ou de mort sur tous ses hommes.   2 - Le Boss assure la sécurité et la protection de ses hommes.   3 - Seul le Boss peut libérer l’un de ses hommes.   4 - Seul le Boss peut décider du châtiment d\'un de ses hommes.   5 - Vous jurerez fidélité et obéissance auprès du Boss et du groupe.   6 - Vous serez dévoués à la cause et participez financièrement à sa cause.   7 - Vous recevrez des tâches à accomplir, vous devrez en assumer seul les conséquences en cas ou vous vous faites attraper.   8 - Chaque personne aura un surnom, pour les actions sous anonymat.   9 - La Red Shadow est avant tout une famille, chaque membre de celle-ci reçoit la protection de l’ensemble du groupe.   10 - Le groupe subviendra au besoin financier des hommes pour toutes amendes obtenues pour notre NIC \'Notre Intérêt Commun\".   En cas de problème en MP, ne répondez pas ou alors renvoyez ce message:  Mon Boss est un véritable dictateur et un joyeux enfoiré, il m\'a donc interdit de communiqué avec la plèbe sous peine de voir mes testicules accrocher à son cou. Pour toutes plaintes, merci de contacter Monsieur McCarty Clifford Boss des Boss, que dieu le préserve.', '2020-12-20 01:41:09', 1),
(3, 'Clifford McCarty', 'Nom: McCarty                                                        Prènom: Clifford  Née: 12 Mai 1863 Montana - Jefferson City            ge: 36 ans  Père: Arnold McCarty                                             Mère: Elizabeth McCarty  Frère: Jack McCarty  Qualités:  Soigneux Stratège Calme  Défaut:  Taciturne Orgueilleux Têtu', '2020-12-20 01:42:58', 1),
(4, 'Encore un Test', 'J\'inscrit un nouveau article \"ticket\" à ma base de donnée via mon formulaire, et je veux qu\'il s\'affiche à mon retour sur l\'index de mon blog.', '2020-12-22 02:45:33', 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nickname` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `nickname`, `password`) VALUES
(1, 'Cathbadix', '$2y$10$.93bktCxdkqO5JC.vPqbCerK/lJIs0oLHHvEmOJqoGq2ADqJppzuq'),
(2, 'mario', '$2y$10$mfBnOWLIfXt8XX2s.y6mBe8Bj4kgKgu0z14TsWw.dwOoU8Ryxi8MK'),
(3, 'Luigi', '$2y$10$V3AyYvhfZMnFxf3Clz9idO0xmTmBj6TCsBe3guy3d9NAfBCdkIr8K');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_commentaires_users1` (`users_id`),
  ADD KEY `fk_commentaires_articles1` (`tickets_id`);

--
-- Index pour la table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_articles_users` (`users_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nickname` (`nickname`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fk_commentaires_articles1` FOREIGN KEY (`tickets_id`) REFERENCES `tickets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_commentaires_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `fk_articles_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
