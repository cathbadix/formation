<?php
session_start();

//si un utilisateur est connecté on le redirige
if (isset($_SESSION['userid'])) {
    header('Location: index.php');
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Connexion</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
    <form action="connexion.php" method="POST">
        <label for="nickname">Pseudo</label>
        <input id="nickname" name="nickname" type="text">
        <label for="password">Mot de passe</label>
        <input id="password" name="password" type="password">
        <input type="submit" value="Connexion">
    </form>
    
</body>
</html>