<?php

//je vérifie que les champs suivants sont prèsent.
if (isset($_POST['nickname']) && isset($_POST['password']) && isset($_POST['password_confirm'])){
    //pour plus de clareté, je range mes variables $_POST dans des variables plus simples
    $nickname = $_POST['nickname'];
    $password = $_POST['password'];
    $password_confirm = $_POST['password_confirm'];
    //je vérifie que les champs ne soit pas vide avec !empty
    if (!empty($nickname) && !empty($password) && !empty($password_confirm)) {
        //je vérifie que le password et le password_confirm soit identique
        if ($password === $password_confirm) {
            //je hash le password afin qu'ils soit indéchiffrable
            $password_hash = password_hash($password, PASSWORD_DEFAULT);
            //je me connecte à la PDO
            try {
                $dbh = new PDO('mysql:host=localhost;dbname=blog; charset=utf8', 'root', '',[
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_EMULATE_PREPARES => false,
                ]);
                } catch (PDOException $j){
                    die("error au secour");
                }


            //je prépare ma requête sql qui inscrira le nouveau utilisateur
            $insert_user = "INSERT INTO `users` (`nickname`, `password`) VALUES (:nickname,:password)";
            //j'enregistre ma préparation dans une variable $stmt
            $stmt = $dbh->prepare($insert_user);
            //j'excute la préparation de ma requête en associant mes variables au values. Ceci pour évité qu'on m'injecte des requête sql.
            $stmt->execute([
                ":nickname" => $nickname,
                ":password" => $password_hash
            ]);
            //je test si la contrainte d'unictié est déclenchée
            if ($stmt->errorInfo()[0] === "23000" && $stmt->errorInfo()[1] === 1062) {
                //on peut donc envoyer un petit message ou rediriger
                echo $nickname . " existe déjà, veuillez en choisir un autre!";
                
            }
            
            header('Location: index.php');


        }else{
            echo "erreur, le mot de passe n'est pas identique";
        }

    }else{
        echo "erreur, tous les champs ne sont pas renseignés!";
    }
}else{
    echo "erreur du formulaire";
}
?>