<?php 
class DatabaseHandler
{
    private $_dbh;
    function __construct()
    {
        $dbname = "blog";
        $dbuser = "root";
        $dbpassword = "";
        
        try {
            $this->_dbh = new PDO("mysql:host=localhost;dbname=" . $dbname . "; charset=utf8;", $dbuser, $dbpassword);
        } catch (PDOException $exception) {
            die("Erreur de connexion à la base de données");
        }
    }

}
$dbh = new DatabaseHandler();