import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Genre } from '../model/genre';

@Injectable({
    providedIn: 'root',
})
export class GenreService {
    constructor(private http: HttpClient) {}

    private apiUrl: string = 'http://localhost:8000/genre';

    getAll(): Observable<Genre[]> {
        return this.http.get<Genre[]>(this.apiUrl);
    }
}
