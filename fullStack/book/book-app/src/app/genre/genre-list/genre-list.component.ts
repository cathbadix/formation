import { Component, OnInit } from '@angular/core';
import { Genre } from 'src/app/model/genre';
import { GenreService } from 'src/app/service/genre.service';

@Component({
    selector: 'app-genre-list',
    templateUrl: './genre-list.component.html',
    styleUrls: ['./genre-list.component.css'],
})
export class GenreListComponent implements OnInit {
    constructor(private genreService: GenreService) {}

    ngOnInit(): void {
        this.getAll();
    }

    getAll(): void {
        this.genreService
            .getAll()
            .subscribe((genres: Genre[]) => console.log(genres));
    }
}
