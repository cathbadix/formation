<?php

namespace App\Controller;

use App\Entity\Format;
use App\Repository\FormatRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/format')]
class FormatController extends AbstractController
{

    #[Route('', name: 'format', methods: ["GET", "HEAD"])]
    public function list(FormatRepository $repository): Response
    {
        $formats = $repository->findAll();
        return $this->json($formats, 200, [], [
            "groups" => "format"
        ]);
    }

    #[Route('/{id}', name: 'format_view', methods: ["GET", "HEAD"])]
    public function view(int $id, FormatRepository $rep): Response
    {
        $format = $rep->find($id);
        if (!$format){
            return $this->json(
                [
                "message" => 'format not found'
                ], 
                404);
        }
        return $this->json($format, 200, [], [
            "groups" => "format"
        ]);
    }

    #[Route('', name: 'format-create', methods:["POST"])]
    public function create(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();
        $format = $serializer->deserialize($data, Format::class, 'json');

        $this->getDoctrine()->getManager()->persist($format);
        $this->getDoctrine()->getManager()->flush();
        
        return $this->json([
            'created' => $format
        ], 201, [], ["groups" => "format"]);
    }

    #[Route('/{id}', name: 'format_delete', methods:["DELETE"])]
    public function delete(int $id, FormatRepository $rep, EntityManagerInterface $em): Response 
    {   
        $format = $rep->find($id);
        if (!$format){
            return $this->json([
                "message" => "format not found"
            ], 404);
        }
        $em->remove($format);
        $em->flush();
        return $this->json([
            "message" => "format deleted successfully"
        ]);
    }

    #[Route('/{id}', name:"format_update", methods:["PUT"])]
    public function update(int $id, Request $request, FormatRepository $rep, SerializerInterface $serializer): Response
    {
        $format = $rep->find($id);
        if (!$format){
            return $this->json([
                "message" => "format not found"
            ], 404);
        }
        $data = $request->getContent();

        $serializer->deserialize($data, Format::class, 'json', [
            'object_to_populate' => $format
        ]);

        $this->getDoctrine()->getManager()->flush();
        return $this->json($format, 200, [], [
            "groups" => "format"
        ]);
    }
}
