<?php

namespace App\Controller;

use App\Entity\Author;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/author')]
class AuthorController extends AbstractController
{
    #[Route('', name: 'author', methods: ["GET", "HEAD"])]
    public function list(AuthorRepository $repository): Response
    {
        $authors = $repository->findAll();
        return $this->json($authors, 200, [], [
            "groups" => "author"
        ] );
    }

    #[Route('/{id}', name: 'author_view', methods: ["GET", "HEAD"])]
    public function view(int $id, AuthorRepository $rep): Response
    {
        $author = $rep->find($id);
        if (!$author){
            return $this->json(
                [
                "message" => 'author not found'
                ], 
                404);
        }
        return $this->json($author, 200, [], [
            "groups" => "author"
        ]);
    }

    #[Route('', name: 'author-create', methods:["POST"])]
    public function create(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();
        $author = $serializer->deserialize($data, Author::class, 'json');

        $this->getDoctrine()->getManager()->persist($author);
        $this->getDoctrine()->getManager()->flush();
        
        return $this->json([
            'created' => $author
        ], 201, [], [
            "groups" => "author"
        ]);
    }

    #[Route('/{id}', name: 'author_delete', methods:["DELETE"])]
    public function delete(int $id, AuthorRepository $rep, EntityManagerInterface $em): Response 
    {   
        $author = $rep->find($id);
        if (!$author){
            return $this->json([
                "message" => "author not found"
            ], 404);
        }
        $em->remove($author);
        $em->flush();
        return $this->json([
            "message" => "author deleted successfully"
        ]);
    }

    #[Route('/{id}', name:"author_update", methods:["PUT"])]
    public function update(int $id, Request $request, AuthorRepository $rep, SerializerInterface $serializer): Response
    {
        $author = $rep->find($id);
        if (!$author){
            return $this->json([
                "message" => "author not found"
            ], 404);
        }
        $data = $request->getContent();

        $serializer->deserialize($data, Author::class, 'json', [
            'object_to_populate' => $author
        ]);

        $this->getDoctrine()->getManager()->flush();
        return $this->json($author, 200, [], [
            "groups" => "author"
        ]);
    }
}
