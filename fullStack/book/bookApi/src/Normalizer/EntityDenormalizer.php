<?php

namespace App\Normalizer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;

class EntityDenormalizer implements ContextAwareDenormalizerInterface {

    private $em;
    public function __construct(EntityManagerInterface $em){
        //on récupère l'entityManager par injection de dépendance 
        //on le fait dans le constructeur car c'est le seul qui est la portée d'un injection dans notre denormalizer 
        $this->em = $em;
    }

    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        //on va chercher une donnée de type $type (notre nom de classe de destination)
        //a partir d'un id qu'on recevra 
        return $this->em->find($type, $data);
    }

    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = [])
    {
        //renvoie vrai si le type est une entité (commence par App\Entity)
        //on vérifie également que les données reçues sont lisibles et utilisables
        //on peut utiliser une représentation de ressource en string, en nombre, ou un tableau associatif avec propriété id précisée 
        return (strpos($type, 'App\\Entity\\') === 0) && (is_numeric($data) || is_string($data) || (is_array($data) && isset($data['id'])) );
    }
}
