<?php
//on récupère notre session
session_start();
//on vérifie qu'un auteur soit bien connecté
if (isset($_SESSION['userid'])) {


    if (isset($_POST['title']) && isset($_POST['content'])) {

        $title = $_POST['title'];
        $content = $_POST['content'];

        if (!empty($title) && !empty($content)) {

            include("db/db.php");
            $dbh = getDatabaseHandler();

            $author = $dbh->getUserById($_SESSION['userid']);
            if ($author) {
                $dbh->createArticle($title, $content, new DateTime(), $author);
            } else {
                //A remplacer par une vraie page d'erreur
                die('Author does not exist');
            }
            header('Location: index.php');
        }
    }
} else {
    //si aucun user n'est trouvé dans la session on redirige vers la page de connexion
    header('Location: sign_in.php');
}
