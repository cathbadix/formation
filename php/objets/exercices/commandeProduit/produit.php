<?php
class Produit {
    private $_nom;
    private $_numerodeproduit;
    private $_prix;

    public function __construct($nom, $numerodeproduit, $prix)
    {
        $this->setNom($nom);
        $this->_numerodeproduit = $numerodeproduit;
        $this->_prix = $prix;
        
    }

    public function setNom($nom)
    {
        if (is_string($nom))
        {
            $this->_nom = $nom;
        }
        else
        {
            return "Ce n'est pas une chaîne de caractère.";
        }        
    }

    public function getNom()
    {
        return $this->_nom;
    }

    public function __toString()
    {
        return "le nom du produit est:" . $this->getNom();
    }
}
$instance1 = new Produit('jambon', 2, 11);
echo $instance1->__toString() . " " . $instance1->getNom();