<?php
require_once('utilisateur.php');
require_once('produit.php');

class BonDeCommande {
    private $_numerodecommande;
    private $_utilisateur;
    private $_produit;
    private $_quantite;
    private $_total;
    
    


    public function __construct($numerodecommande, $utilisateur, $produit, $quantite, $total)
    {
        $this->_numerodecommande = $numerodecommande;
        $this->_utilisateur = $utilisateur;
        $this->_produit = $produit;
        $this->_quantite = $quantite;
        $this->_total = $total;
        
    }   


    public function __toString()
    {
        return "{$this->_numerodecommande} {$this->_quantite} {$this->_total} {$this->_utilisateur} {$this->_produit}";
    }
}
$utilisateur = new Utilisateur('Fossier', 'Victorien', 'Chemin de St Catherine', '20%');
$produit = new Produit('Savon', 'n°115', '4');
$bondecommande = new BonDeCommande('n°1', $utilisateur, $produit, '3', $total);
echo $bondecommande;



