<?php
class Utilisateur {
    private $_nom;
    private $_prenom;
    private $_adresse;
    private $_reduction;

    public function __construct($nom, $prenom, $adresse, $reduction)
    {
        $this->_nom = $nom;
        $this->_prenom = $prenom;
        $this->_adresse = $adresse;
        $this->_reduction = $reduction;
        
    }

    public function __toString()
    {
        return "{$this->_nom} {$this->_prenom} {$this->_adresse} {$this->_reduction}";
    }
}
