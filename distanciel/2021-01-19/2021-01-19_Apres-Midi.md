# Après Midi du 19 Janvier 2021

## Utilisateurs et Security

[Voir la documentation officielle.](https://symfony.com/doc/current/security.html)

Notre application doit se charger de gérer des utilisateurs et leur connexion à la plateforme, et en ce faisant accéder à des fonctionnalités supplémentaires (par exemple poster un article, l'éditer, le supprimer...).

Pour ça, il faut comprendre ce qu'est l'extension Security dans symfony.

### Le module Security

Tout comme le `Router` se met entre les requêtes entrantes et le reste de l'application pour les diriger vers le bon controller, `Security` se place entre la requête routée et le controller et agit comme un filet supplémentaire à traverser.

![schema security 1](security1.png "L'ordre d'interception d'une requête")

En passant par `Security`, on obtient certaines fonctionnalités utiles; comme par exemple une gestion d'autorisation d'accès à certaines parties du site, l'utilisation de roles pour les utilisateurs octroyant des droits, etc.

Pour que notre gestion des utilisateurs soit compatible avec `Security` et donc profite de ces fonctionnalités, il faut implémenter certaines interfaces, et consigner certaines options dans la configuration de `Security`.

Heureusement, en utilisant le `maker-bundle`, ces choses vont être faites pour nous.

### Création d'une entité `User` compatible avec `Security`

On pourrait créer notre classe `User` avec `make:entity` et la rendre compatible ensuite avec `Security`, mais on va plutôt utiliser `make:user` qui permet de créer une classe déjà compatible.

```console
php bin/console make:user
```

Certaines questions nous sont posées, on y répond, et un notre `App\Entity\User` est créé, ainsi qu'un `App\Repository\UserRepository`.

Un fichier de configuration est également altéré, `config/packages/security.yaml`, il s'agit du fichier dictant son comportement au module `Security`.

Différentes choses y sont consignées, comme le paramétrage de _firewalls_ (_pare-feu_), qui sont un ensemble de règle permettant de cloisonner certaines parties de notre site sous conditions.

Mais ce qui y a été ajouté ce sont les mentions d'un `user_provider`.

#### Qu'est ce que le `user_provider` et d'où vient-il ?

Le _user provider_ (_fournisseur d'utilisateurs_) est une classe permettant, à chaque fois que la requête est routée, le chargement de l'objet `User` à partir de la session.

Notre provider par défaut se charge de vérifier si l'utilisateur n'est pas daté en faisant une requête en base de données (avec l'aide de Doctrine).

Le provider se charge également de désauthentifier l'utilisateur si celui ci se déconnecte.

On peut en créer des personnalisés, mais celui par défaut nous convient très bien.

Notre `make:user` nous permet d'utiliser ce provider pour _fournir_ nos entités `User` grâce aux paramètres rajoutés dans `config/packages/security.yaml`.

### Inscription d'un utilisateur

Pour inscrire un utilisateur il faut que celui ci soit enregistré dans la base, et son mot de passe haché.

Pour ce faire, on peut utiliser une autre commande :

```console
php bin/console make:registration-form
```

Cette commande permettra la création d'un formulaire d'inscription et d'un controller capable de le servir.

Rien de bien nouveau à ce niveau, si ce n'est l'utilisation d'un encodeur de mot de passe.

En effet, dans le `RegistrationController` créé par notre `maker-bundle`, on trouve la mention d'un `UserPasswordEncoderInterface`.

```php
/**
 * @Route("/register", name="app_register")
 */
public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
{
    $user = new User();
    $form = $this->createForm(RegistrationFormType::class, $user);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
    // encode the plain password
    $user->setPassword(
        $passwordEncoder->encodePassword(
            $user,
            $form->get('plainPassword')->getData()
            )
        );

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
        // do anything else you need here, like send an email

        return $this->redirectToRoute('index');
    }

    return $this->render('registration/register.html.twig', [
        'registrationForm' => $form->createView(),
    ]);
}
```

En appelant un représentant de `UserPasswordEncoderInterface` dans les paramètres de notre méthode de controller, on demande à Symfony de nous fournir un encodeur de mot de passe valide.

Ici, il s'agit de celui par défaut.

En utilisant ensuite sa méthode `encodePassword()` cela nous permet de hacher un mot de passe utilisateur.

#### Le formulaire `RegistrationFormType`

Un autre ajout de cette commande `make:registration-form` est la création d'un formulaire, séparé cette fois, dans un fichier `src/Form/RegistrationFormType.php`.

Celui ci ressemble à notre création de formulaire classique, excepté qu'il est réutilisable dans plusieurs controllers.

Le formulaire est ensuite récupéré et utilisé dans le `RegistrationController`, comme d'habitude.

### Connexion d'un utilisateur

Pour connecter un utilisateur, la tâche est un tout petit peu plus complexe, car devant passer par un `Authenticator`.

Cependant, il n'en est pas moins facile de le réaliser à l'aide du `maker-bundle` :

```console
php bin/console make:auth

 What style of authentication do you want? [Empty authenticator]:
  [0] Empty authenticator
  [1] Login form authenticator
 > 1

 The class name of the authenticator to create (e.g. AppCustomAuthenticator):
 > LoginFormAuthenticator

 Choose a name for the controller class (e.g. SecurityController) [SecurityController]:
 >

 Do you want to generate a '/logout' URL? (yes/no) [yes]:
 >

 created: src/Security/LoginFormAuthenticator.php
 updated: config/packages/security.yaml
 created: src/Controller/SecurityController.php
 created: templates/security/login.html.twig


  Success!


 Next:
 - Customize your new authenticator.
 - Finish the redirect "TODO" in the App\Security\LoginFormAuthenticator::onAuthenticationSuccess() method.
 - Review & adapt the login template: templates/security/login.html.twig.
```

Décortiquons ensemble ce qui a été créé/modifié pour que ça fonctionne :
